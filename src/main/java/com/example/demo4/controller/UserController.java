package com.example.demo4.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.model.User;
import com.example.demo4.service.UserService;



@RestController
public class UserController {
	
	@Autowired
	private UserService userService;

	@PostMapping(value = "/user")
	public ResponseEntity<User> addUser(@Valid @RequestBody User user) {
		User user1 = userService.createUser(user);
		return new ResponseEntity<User>(user1, HttpStatus.OK);

	}
	
	@GetMapping(value = "/users")
	public ResponseEntity<List<User>> getAllUsers(User user) {
		List<User> users = userService.getAllUsers();
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

}
