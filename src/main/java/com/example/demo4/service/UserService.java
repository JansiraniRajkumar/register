package com.example.demo4.service;

import java.util.List;

import com.example.demo4.model.User;


public interface UserService {
	public List<User> getAllUsers();
	public User createUser(User user);
	public String login(int customerId,String password);

}
