package com.example.demo4.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo4.model.User;
import com.example.demo4.repository.UserRepository;



@Service
@Transactional
public class UserServiceImpl implements UserService {
  
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User createUser(User user) {
		System.out.println(user.getCustomerName());
		return userRepository.save(user);
	}
	
	@Override
	public List<User> getAllUsers() {
		List<User> userList = (List<User>) userRepository.findAll();

		if (userList.size() > 0) {
			return userList;
		} else {
			return new ArrayList<User>();
		}
	}

	

}
