package com.example.demo4.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;
	@Column(unique=true)
	private long aaharNumber;
	private String password;
	private String customerName;
	private int phoneNo;
	private String ocupation;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int customerId, long aaharNumber, String password, String customerName, int phoneNo, String ocupation) {
		super();
		this.customerId = customerId;
		this.aaharNumber = aaharNumber;
		this.password = password;
		this.customerName = customerName;
		this.phoneNo = phoneNo;
		this.ocupation = ocupation;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public long getAaharNumber() {
		return aaharNumber;
	}
	public void setAaharNumber(long aaharNumber) {
		this.aaharNumber = aaharNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(int phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getOcupation() {
		return ocupation;
	}
	public void setOcupation(String ocupation) {
		this.ocupation = ocupation;
	}
	@Override
	public String toString() {
		return "User [customerId=" + customerId + ", aaharNumber=" + aaharNumber + ", password=" + password
				+ ", customerName=" + customerName + ", phoneNo=" + phoneNo + ", ocupation=" + ocupation + "]";
	}


}
